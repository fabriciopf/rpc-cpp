#!/bin/bash
echo -e "### Deploying CustomAMI Stack..."
aws cloudformation deploy --stack-name CustomAMI --template-file CreateAMI.yaml --capabilities CAPABILITY_IAM
echo -e "### Deploying KVService Stack..."
aws cloudformation deploy --stack-name KVService --template-file KVService.yaml --capabilities CAPABILITY_IAM
